Pardus ARM
======
**Pardus ARM** projesi LS Bilişim Sistemleri Ticaret Sanayi Ticaret Limited Şirketi ve Pardus Topluluğu iş birliğinde yürütülen açık kaynak bir projedir.
Proje kapsamında Pardus ARM a özel yazılımlar özgür lisanslar ile lisanslanmıştır. Bu bağlamda projedeki yazılımları kullanırken lisans dosyasını okumanız faydanıza olacaktır.
Projenin ana sayfası olan www.pardusarm.com adresinde ve Bitbucket adresiizde tüm sürüm değişikliklerini bulabilirsiniz.

Proje ile ilgili her türlü sorunlarınızı  **mehmet@ls.com.tr** ve **mehmet@pardus.net.tr** mail adresine iletebilirsiniz.


#### Developer/Company
* Homepage: **http://www.pardusarm.com**  **http://www.pardus.net.tr**
* e-mail:   mehmet@ls.com.tr, erdogan.bilgici@pardus.net.tr, destek@linuxcozumleri.com  
* Authors: Mehmet Nuri ÖZTÜRK , Erdoğan BİLGİCİ

